const express = require('express');
const router = express.Router();
const db = require('./db');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const rn = require('random-number');
const nodeMailer = require('nodemailer');

const transporter = nodeMailer.createTransport({
    service: 'gmail',
    host:'http://localhost:3000',
    port:3000,
    secure:false,
    auth: {
        user: 'prince.vinod5a8@gmail.com',
        pass: 'goabeach'
    },
    tls:{
        rejectUnauthorized:false
    }
});

const gen = rn.generator({
    min:  524201
    , max:  835368
    , integer: true
})

const SECRET_KEY = "JWT_SECRET";

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});


router.get('/getUsers', (req, res) => {
  console.log(req.body);
  db.User.findAll().then( resData => {
    let usersData = {};
    usersData["data"] = resData;
    res.send(usersData);
  })
});

let data={};

router.post('/user/register',(req,res)=>{
console.log(req.body);
db.User.findOne({
    where: {
        email:req.body.email
    }
}).then(user=>{
    if(user){
        res.send({"statusCode":409,"message":"User Already Existed with that Email Address !"});
    }else{
        createUser(req,res);
    }
});
})

router.post('/users/authenticate',(req,res)=>{
    console.log("----backend--------------")
    db.User.findOne({
        where: {
            email: req.body.email
        }
    }).then(user => {
        if (!user) {
            return res.status(404).send({"statusCode":404,"message":"User Not Found."});
        }
        var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
        if (passwordIsValid) {
            var token = generateToken(user);
            console.log(token);
            // Change everyone without a last name to "Doe"
           storeAuthToken(req,res,user);
        } else {
            return res.status(401).send({"statusCode":401,"message":"Invalid Password Try Again !"});
        }
    }).catch(err => {
        res.status(404).send({"statusCode":404,"message":'User Not Found.'});
    });
})


router.post('/users/sigInWithGoogle',(req,res) =>{
    db.User.findOne({
        where: {
            email: req.body.email,
        }
    }).then(existedUser => {
        if (!existedUser) {
           db.User.create({
               firstName : req.body.firstName,
               lastName : req.body.lastName,
               email: req.body.email,
               authToken: req.body.authToken,
               provider: req.body.provider
           }).the(userCreatedData =>{
               res.send({'status': 200, "user":JSON.stringify(userCreatedData)});
           })
        }
        res.send({'status': 200, "user":JSON.stringify(existedUser)});
    })

})

function generateToken(user) {
    return  token = jwt.sign({ id: user.id }, SECRET_KEY, {
        expiresIn: 86400 // expires in 24 hours
    });
}

function createUser(req,res) {
    console.log(req.body)
    db.User.create({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        password:bcrypt.hashSync(req.body.password),
        email:req.body.email,
        mobileNumber:req.body.mobileNumber,
        provider: "LEDGURE"
    }).then(userData =>{
        res.send({"statusCode":200,"message":"User registration successful!"});
    })
}

function storeAuthToken(req,res,user){
    db.User.update({ authToken: token }, {
        where: {
            id: user.id
        }
    }).then((tokenUpdated) => {
        db.User.findOne({where:{id:user.id}}).then(userResponse =>{
            res.status(200).send({"statusCode":200,"message":"User LoggedIn Successfully!","userData":userResponse});
        })
    });
}

router.post('/user/fogotPassword',(req,res) =>{
    db.User.findOne({where:{email:req.body.email}}).then(userExisted =>{
        console.log(JSON.stringify(userExisted))
        if(!userExisted){
            res.send({"status":204,"message": "Please enter valid registered email address!"})
        }else {
            const randomOtp = gen();
            let mailOptions = {
                from: 'prince.vinod5a8@gmail.com',
                to: req.body.email,
                subject: 'Ledgure Reset Password',
                text: 'Hello' + getFullName(userExisted) + ' !,Otp for your password reset is:' + randomOtp
            };
            console.log('mail options' + JSON.stringify(mailOptions));
            transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                    res.send({"status":500,"message":"Something went wrong with mail sending !"})
                } else {
                    saveOtp(userExisted,res,randomOtp)
                }
            });
        }
    })
})

function getFullName(user){
    return user.firstName + ' ' + user.lastName;
}

function saveOtp(user,res,randomOtp) {
    db.Otp.findOne({userId:user.id}).then(userExisted => {
        if(userExisted)
            updateOtp(user,randomOtp,res);
        else
            createOtp(user,randomOtp,res);
    })

}

router.post('/user/resetPassword',(req,res) =>{
    db.Otp.findOne({where:{userId:req.body.userId}}).then(otpObj => {
        console.log(JSON.stringify(otpObj))
        if(otpObj.generatedOtp == req.body.otp){
            db.User.update({password: bcrypt.hashSync(req.body.newPassword)},{where:{id:req.body.userId}}).then(updatedUser => {
                if(updatedUser)
                    res.send({status:200,message:"Your Password Reset Was Done Successfully!"})
                else
                    res.send({status:204,message:"Something went wrong!"})
            })
        }else{
            res.send({status:403,message:"Try again with Valid OTP ! "})
        }
    })

})

function createOtp(user,randomOtp,res) {
    db.Otp.create({
        userId:user.id,
        generatedOtp:randomOtp
    }).then(userOtp => {
        res.send({"status":200,"message": "Email Sent to your registered email address","userId":user.id})
    })
}

function updateOtp(user,randomOtp,res){
    db.Otp.update({generatedOtp:randomOtp},{where:{userId:user.id}}).then(userOtp =>{
        res.send({"status":200,"message": "Email Sent to your registered email address","userId":user.id})
    })
}

module.exports = router;
