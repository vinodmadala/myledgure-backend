const Sequelize = require('sequelize');

const connection = new Sequelize('ledgure','root','root',{
    host:'localhost',
    dialect:'mysql',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
});

var User = connection.define('user', {
    firstName: {
      type: Sequelize.STRING,
      default:null
    },
    lastName: {
      type: Sequelize.STRING,
      default:''
    },
    password: {
      type: Sequelize.STRING,
      default:null
    },
    email: {
        type: Sequelize.STRING,
        default:null
    },
    mobileNumber: {
        type: Sequelize.STRING,
        default:null
    },
    gender: {
        type:Sequelize.STRING,
        default:null
    },
    dateOfBirth: {
        type:Sequelize.STRING,
        default:null
    },
    authToken: {
      type: Sequelize.STRING,
      default:null
    },
    provider: {
        type: Sequelize.STRING,
        default:null
    },
    deletedAt: {
      type: Sequelize.DATE,
      default:null
    }
  });

var Otp = connection.define('otp',{
    userId:{
        type: Sequelize.BIGINT
    },
    generatedOtp:{
        type: Sequelize.STRING
    },
    deletedAt: {
        type: Sequelize.DATE,
        default:null
    }
});
  
  connection.sync({
  });

  exports.User = User;
  exports.Otp = Otp;
